class HashTable:
    def __init__(self, size=10):
        self.size = size
        # Inicjalizacja tablicy (będącej listą list dla obsługi kolizji metodą łańcuchową)
        self.table = [[] for _ in range(self.size)]
        # print(self.table)

    def hash_function(self, key):
        # Metoda do obliczania indeksu w tablicy dla danego klucza
        hash_value = hash(key) % self.size
        return hash_value

    def insert(self, key, value):
        # Metoda do dodawania pary klucz-wartość do tablicy haszującej
        insert_index = self.hash_function(key)

        key_exist = False
        bucket = self.table[insert_index]
        for i, kv in enumerate(bucket):
            k, v = kv
            if key == k:
                key_exist = True
                break
        if key_exist:
            bucket[i] = ((key, value))
        else:
            bucket.append((key, value))

    def remove(self, key):
        # Metoda do usuwania pary klucz-wartość na podstawie klucza
        insert_index = self.hash_function(key)
        if self.table[insert_index] is not None:
            for item in self.table[insert_index]:
                if item[0] == key:
                    self.table[insert_index].remove(item)
            print("key not found")
        else:
            print("key not found")

    def search(self, key):
        # Metoda do wyszukiwania wartości na podstawie klucza
        insert_index = self.hash_function(key)
        if self.table[insert_index] is not None:
            for item in self.table[insert_index]:
                if item[0] == key:
                    return item[1]  # zwraca wartosc
        return None



    def contains(self, key):
        # Metoda do sprawdzania, czy klucz znajduje się w tablicy
        insert_index = self.hash_function(key)

        for item in self.table[insert_index]:
            if item[0] == key:
                return True
        return False

    def count_elements(self):
        # Metoda do zwracania liczby elementów w tablicy
        elements = 0
        for lists in self.table:
            elements += len(lists)
        return elements


# Przykład użycia (będzie wymagał implementacji metod)
hash_table = HashTable(size=10)
hash_table.insert("klucz1", "wartość1")
print(hash_table.search("klucz1"))
hash_table.remove("klucz1")
print(hash_table.contains("klucz1"))
print(hash_table.count_elements())